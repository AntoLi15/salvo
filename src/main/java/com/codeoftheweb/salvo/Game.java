package com.codeoftheweb.salvo;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity


public class Game {

    @Id //Carga una PrimaryKey (identificador, clave primaria)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native") //Autogenera un valor
    @GenericGenerator(name = "native", strategy = "native")
    private long id; //Crea la clave id privada (para no permitir acceso)
    private Date creationDate; //Creacion de la fecha privada (limita acceso)

    @OneToMany(mappedBy = "game", fetch = FetchType.EAGER) //MappedBy porque se envia desde la misma clase(?
    private Set<GamePlayer> gamePlayers; //Relacion de un muchos


    @OneToMany(mappedBy = "player", fetch = FetchType.EAGER)
    private Set<Score> scores;

    public Game() { //constructor vacio
    }


    public Game(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    //Getters y setter de los campos creados

    public Date getCreationDate()
    {
        return creationDate;
    }

    public long getId()
    {
        return id;
    }

    public Set<GamePlayer> getGamePlayers()
    {
        return gamePlayers;
    }

    public void setCreationDate(Date creationDate)
    {
        creationDate = creationDate;
    }

    @JsonIgnore
    public void addGamePlayer(GamePlayer gameplayer) {
        gameplayer.setGame(this);
        gamePlayers.add(gameplayer);
    }
}


