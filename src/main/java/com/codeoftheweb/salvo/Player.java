package com.codeoftheweb.salvo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Player {
    @Id //Clave primaria
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native") //genera un valor automatico
    @GenericGenerator(name = "native", strategy = "native")
    private long id; //se crea una clase primaria ID para que no se pueda acceder

    private String userName; //se define una variable  Tipo Variable//
    private String name;
    private String password;


    @OneToMany(mappedBy = "player", fetch = FetchType.EAGER)
    private Set<GamePlayer> gamePlayers; //

    //Desde donde estoy hacia donde voy tiene una sintaxis especial

    @OneToMany(mappedBy = "player", fetch = FetchType.EAGER)
    private Set<Score> scores;
    //no se pone join porque solo va por el manytomany (crea una columna)


    public Player() {
    }

    public Player(String userName, String password) { //llamo al objeto player

        this.userName = userName;
        this.password= password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @JsonIgnore
    public void addGamePlayer(GamePlayer gameplayer) {
        gameplayer.setPlayer(this);
        gamePlayers.add(gameplayer);
    }

    public float getWins() {
        return scores
                .stream()
                .filter(score -> score.getScore() == 1)
                .count();
    }

    public float getDraws() {
        return scores
                .stream()
                .filter(score -> score.getScore() == 0.5)
                .count();
    }

    public float getLoses() {
        return scores
                .stream()
                .filter(score -> score.getScore() == 0)
                .count();
    }


    //Crear SubClase para darle a Spring un método para obtener esa información
    @SpringBootApplication
    public class Application extends SpringBootServletInitializer{}


    @Configuration
    class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {}

    }





