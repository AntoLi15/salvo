package com.codeoftheweb.salvo;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.util.Date;


@Entity
public class Score {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    private float score;

    //Relacion de muchos a uno

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="game_id")
    private Game game;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="player_id")
    private Player player;

    private Date finishDate;

    public Score() { }


    //Agregamos los parametros necesarios para Score

    public Score(Date finishDate, Game game, Player player, float score)
    {
            this.setFinishDate(finishDate);
            this.setGame (game);
            this.setPlayer (player);
            this.setScore (score);
    }

    public float getScore()
    {
        return score;
    }
    public void setScore(float score)

    {
        this.score = score;
    }

    public long getId()
    {
        return id;
    }

    public Game getGame()
    {
        return game;
    }

    public void setGame(Game game)
    {
        this.game = game;
    }

    public Player getPlayer()
    {
        return player;
    }

    public void setPlayer(Player player)

    {
        this.player = player;
    }


    public Date getFinishDate()
    {
        return finishDate;
    }

    public void setFinishDate(Date finishDate)

    {
        this.finishDate = finishDate;
    }
}



