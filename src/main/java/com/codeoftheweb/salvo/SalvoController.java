package com.codeoftheweb.salvo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class SalvoController {

    @Autowired
    private GameRepository gameRepository;

    //autorizo el gamePlayer
    @Autowired
    private GamePlayerRepository gamePlayerRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ShipRepository shipRepository;

    @Autowired
    private  SalvoRepository salvoRepository;

    //Permite la ejecucion del codigo mediante la ruta especificada

    @RequestMapping("/games")
    public Map<String, Object> makeDTO(Authentication authentication) {
        Map<String, Object> dto = new LinkedHashMap<>();
        if (authentication == null || authentication instanceof AnonymousAuthenticationToken)
            dto.put("player", "Guest");
        else dto.put("player", loggedPlayerDTO(playerRepository.findByUserName(authentication.getName())));
        dto.put("games", getAll());
        return dto;
    }

    public List<Object> getAll() {
        return gameRepository
                .findAll()
                .stream()
                .map(game -> gameToDto(game))
                .collect(Collectors.toList());
    }


    @RequestMapping(path = "/games", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> getCreateGame(
            Authentication auth) {

        if (auth.getName() == null || auth instanceof AnonymousAuthenticationToken)
            return new ResponseEntity<>(makeMap("error", "Unauthorized"), HttpStatus.UNAUTHORIZED);


        Game game = new Game();

        gameRepository.save(game);

        Date date = new Date();

        Player player = playerRepository.findByUserName(auth.getName());

        GamePlayer gp = new GamePlayer(date, game, player);

        gamePlayerRepository.save(gp);

        return new ResponseEntity<>(makeMap("gpid", gp.getId()), HttpStatus.CREATED);
    }


    public Map<String, Object> loggedPlayerDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", player.getId());
        dto.put("name", player.getUserName());
        return dto;
    }


    @RequestMapping(path = "/game_view/{id}", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> getGameView(
            Authentication auth, @PathVariable long id) {

        Player player = playerRepository.findByUserName(auth.getName());
        GamePlayer gamePlayer = gamePlayerRepository.findById(id).orElse(null);

        if (gamePlayer == null) {
            return new ResponseEntity<>(makeMap("error", "Fobidden"), HttpStatus.FORBIDDEN);
        }

        if (player.getId() == gamePlayer.getPlayer().getId()) {
            return new ResponseEntity<>(makeMap("error", "Unauthorized"), HttpStatus.UNAUTHORIZED);
        }


        if (gamePlayerRepository.findById(id) == null) {
            return new ResponseEntity<>(makeMap("error", "Game not found"), HttpStatus.FORBIDDEN);
        }


        if (gamePlayerRepository.findById(id).get().getPlayer().getUserName() == auth.getName()) {
            return new ResponseEntity<>(gameViewDTO(gamePlayerRepository.findById(id).get()), HttpStatus.CREATED);
        }

        return new ResponseEntity<>(makeMap("error", "Unauthorized"), HttpStatus.UNAUTHORIZED);

    }

    private Map<String, Object> makeMap(String key, Object value) {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put(key, value);
        return map;
    }


    @RequestMapping(path = "/players", method = RequestMethod.POST)
    public ResponseEntity<Object> register(

            @RequestParam String username, @RequestParam String password) {

        if (username.isEmpty() || password.isEmpty()) {
            return new ResponseEntity<>("Missing data", HttpStatus.FORBIDDEN);
        }

        if (playerRepository.findByUserName(username) != null) {
            return new ResponseEntity<>("Name already in use", HttpStatus.FORBIDDEN);
        }

        playerRepository.save(new Player(username, passwordEncoder.encode(password)));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping("/leaderboard")
    public List<Object> getLeaderboard() {

        return playerRepository
                .findAll()
                .stream()
                .map(player -> leaderboardDTO(player))
                .collect(Collectors.toList());

    }

    private Map<String, Object> leaderboardDTO(Player player) {

        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("username", player.getUserName());
        dto.put("score", scoreToDto(player));
        return dto;
    }


    private Map<String, Object> gameViewDTO(GamePlayer gamePlayer) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", gamePlayer.getGame().getId());
        dto.put("creationDate", gamePlayer.getGame().getCreationDate());
        dto.put("gamePlayers", getGamePlayerList(gamePlayer.getGame().getGamePlayers()));
        dto.put("ships", getShipList(gamePlayer.getShips()));
        dto.put("salvoes", obtainAllSalvoes(gamePlayer));
        return dto;
    }

    //creo la función de Mapeo para player
    private Map<String, Object> playerToDto(Player player) {

        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", player.getId());
        dto.put("username", player.getUserName());
        dto.put("score", scoreToDto(player));
        return dto;
    }

    //creo la función de Mapeo para gamePlayer
    private Map<String, Object> gamePlayerToDto(GamePlayer gamePlayer) {

        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", gamePlayer.getId());
        dto.put("player", playerToDto(gamePlayer.getPlayer()));
        return dto;

    }

    //Creo la función de mapeo para game
    private Map<String, Object> gameToDto(Game game) {

        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", game.getId());
        dto.put("creationDate", game.getCreationDate());
        dto.put("gamePlayers", getGamePlayerList(game.getGamePlayers()));
        return dto;
    }

    private Map<String, Object> shipToDto(Ship ship) {

        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("type", ship.getShipType());
        dto.put("locations", ship.getLocationList());
        return dto;

    }

    private Map<String, Object> salvoToDto(Salvo salvo) {

        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("turn", salvo.getTurn());
        dto.put("player", salvo.getGamePlayer().getPlayer().getId());
        dto.put("locations", salvo.getLocationList());
        return dto;
    }

    private Map<String, Object> scoreToDto(Player player) {

        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("total", player.getWins() + player.getDraws() * 0.5);
        dto.put("win", player.getWins());
        dto.put("lost", player.getLoses());
        dto.put("tied", player.getDraws());
        return dto;
    }


    //Obtengo una lista a partir de un SET
    private List<Map<String, Object>> getGamePlayerList(Set<GamePlayer> gamePlayers) {
        return gamePlayers.stream().map(gamePlayer -> gamePlayerToDto(gamePlayer))
                .collect(Collectors.toList());
    }

    private List<Map<String, Object>> getShipList(Set<Ship> ships) {
        return ships.stream().map(ship -> shipToDto(ship))
                .collect(Collectors.toList());
    }


    private List<Map<String, Object>> obtainAllSalvoes(GamePlayer gamePlayer) {

        List<Map<String, Object>> salvoList = new ArrayList<>();
        gamePlayer.getGame().getGamePlayers().forEach(gpl -> gpl.getSalvoes().forEach(salvo -> salvoList.add(salvoToDto(salvo))));
        return salvoList;
    }

    @RequestMapping(path = "/games/players/{gamePlayerId}/ships")//, method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> getPleaceShip(Authentication auth, @PathVariable long gamePlayerId, @RequestBody Set<Ship> ships) {

        if (auth == null || auth instanceof AnonymousAuthenticationToken)
            return new ResponseEntity<>(makeMap("error", "Unauthorized"), HttpStatus.UNAUTHORIZED);

        GamePlayer gameplayer = gamePlayerRepository.findById(gamePlayerId).get();

        if (playerRepository.findByUserName(auth.getName()).getUserName() != gameplayer.getPlayer().getUserName())

            return new ResponseEntity<>(makeMap("error", "Unauthorized"), HttpStatus.UNAUTHORIZED);


        //Si no hay nadie logueado, si el gameplayer no existe, o si esta logueado pero el ID no corresponde, me tira error)

        if (gameplayer.getShips().size() >= 5)
            return new ResponseEntity<>(makeMap("error", "Forbidden"), HttpStatus.FORBIDDEN);


        for (Ship ship : ships) {
            ship.setGamePlayer(gameplayer);
            shipRepository.save(ship);
        }

        return new ResponseEntity<>(makeMap("ok", "Ships Placed"), HttpStatus.CREATED);
    }



    @RequestMapping(value = "/games/players/{id}/salvos", method = RequestMethod.POST)
    public ResponseEntity<String> AgregaSalvo(Authentication auth, @PathVariable long id, @RequestBody Salvo salvo) {

        if (auth == null || auth instanceof AnonymousAuthenticationToken)//si el usuario logueado es null o si es anonimo, esta desautorizado
            return new ResponseEntity<>("Unauthorized", HttpStatus.UNAUTHORIZED);

        if (gamePlayerRepository.findById(id) == null) //Si el Id obtenido de el repositorio de juegos es nulo, entonces esta desautorizado
            return new ResponseEntity<>("Unauthorized", HttpStatus.UNAUTHORIZED);

        GamePlayer gamePlayer = gamePlayerRepository.findById(id).get();

        if (gamePlayer.getPlayer().getUserName() != auth.getName())
            return new ResponseEntity<>("Unauthorized", HttpStatus.UNAUTHORIZED);

        List<Salvo> salvoList = gamePlayer.getSalvoes().stream().filter(salvo2 -> salvo2.getTurn() == salvo.getTurn()).collect(Collectors.toList());

        if (salvoList.stream().anyMatch(salvo2 -> salvo2.getGamePlayer().getId() == gamePlayer.getId()))
            return new ResponseEntity<>("Forbidden", HttpStatus.FORBIDDEN);

        gamePlayer.getSalvoes().add(salvo);

        salvo.setGamePlayer(gamePlayer);

        salvoRepository.save(salvo);

        return new ResponseEntity<>("created", HttpStatus.CREATED);
    }

}

