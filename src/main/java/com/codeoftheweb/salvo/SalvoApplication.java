package com.codeoftheweb.salvo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


//Punto de partida del programa, aca es donde se guarda la persistencia y el hardcodeo

@SpringBootApplication
public class SalvoApplication {

	//punto de partida de la clase estatica
	public static void main(String[] args) {
		SpringApplication.run(SalvoApplication.class, args);
	}

	//con la anotacion Bean spring se encarga de configurar los recursos de cuando se instancia los objetos y como interactuan.


	@Bean
	public CommandLineRunner initData(ShipRepository shipRepository, PlayerRepository playerrepository, GameRepository gamerepository, GamePlayerRepository gamePlayerRepository, SalvoRepository salvoRepository, ScoreRepository scoreRepository) {
		return (args) -> {
			//creo todos los jugadores
			Player p1 = new Player("Jon@snow.com", passwordEncoder().encode("snow"));
			Player p2 = new Player("Arya@stark.com", passwordEncoder().encode("stark"));
			Player p3 = new Player("Bran@stark.com", passwordEncoder().encode("summer"));
			Player p4 = new Player("Drogo@khal.com", passwordEncoder().encode("dany"));
			//creo todas las nuevas fechas y los juegos con esas fechas creadas
			Date f1 = new Date();
			Game g1 = new Game(f1);
			Date f2 = new Date();
			Game g2 = new Game(f2);
			Date f3 = new Date();
			Game g3 = new Game(f3);
			Date f4 = new Date();
			Game g4 = new Game(f4);

			//creo todos los gamePlayer con los players, la fecha y el juego
			GamePlayer gp1 = new GamePlayer(f1, g1, p1);
			GamePlayer gp2 = new GamePlayer(f1, g1, p2);
			GamePlayer gp3 = new GamePlayer(f2, g2, p3);
			GamePlayer gp4 = new GamePlayer(f2, g2, p4);

			//GUARDO todos los datos creados
			playerrepository.save(p1);
			playerrepository.save(p2);
			playerrepository.save(p3);
			playerrepository.save(p4);

			gamerepository.save(g1);
			gamerepository.save(g2);
			gamerepository.save(g3);
			gamerepository.save(g4);

			gamePlayerRepository.save(gp1);
			gamePlayerRepository.save(gp2);
			gamePlayerRepository.save(gp3);
			gamePlayerRepository.save(gp4);

			// Crear las locations y el ship
			List<String> locations1 = new ArrayList<>();
			locations1.add("A1");
			locations1.add("A2");
			locations1.add("A3");

			/*List<String> location2 = new ArrayList<>();
			location2.add("D1");
			location2.add("D2");
			location2.add("D3");*/

			Ship sp1 = new Ship("Carrier", gamePlayerRepository.getOne((long) 1), locations1);
			shipRepository.save(sp1);

			//String variable = "Carrier";

			Ship sp2 = new Ship("Carrier", gamePlayerRepository.getOne((long) 2), locations1);
			shipRepository.save(sp2);


			List<String> locationsSalvo1 = new ArrayList<>();
			locationsSalvo1.add("C1");
			Salvo salvo1 = new Salvo(1, gp1, locationsSalvo1);
			salvoRepository.save(salvo1);

			List<String> locationsSalvo2 = new ArrayList<>();
			locationsSalvo2.add("A6");
			Salvo salvo2 = new Salvo(1, gp1, locationsSalvo2);
			salvoRepository.save(salvo2);


			List<String> locationsSalvo3 = new ArrayList<>();
			locationsSalvo3.add("B2");
			Salvo salvo3 = new Salvo(1, gp1, locationsSalvo3);
			salvoRepository.save(salvo3);


			Date finishDate1 = new Date();
			Date finishDate3 = new Date();
			Date finishDate2 = new Date();


			Score s1 = new Score(finishDate1, g1, p1, 1);
			Score s2 = new Score(finishDate1, g1, p2, 1);
			Score s3 = new Score(finishDate2, g2, p1, (float) 0.5);
			Score s4 = new Score(finishDate2, g2, p2, 1);
			Score s5 = new Score(finishDate3, g3, p3, 1);
			Score s6 = new Score(finishDate3, g3, p4, 1);
			Score s7 = new Score(finishDate3, g4, p3, (float) 0.5);
			Score s8 = new Score(finishDate3, g4, p4, 1);

			scoreRepository.save(s1);
			scoreRepository.save(s2);
			scoreRepository.save(s3);
			scoreRepository.save(s4);
			scoreRepository.save(s5);
			scoreRepository.save(s6);
			scoreRepository.save(s7);
			scoreRepository.save(s8);
		};
	}

	@Bean
	public PasswordEncoder passwordEncoder() {return PasswordEncoderFactories.createDelegatingPasswordEncoder();							}

}
@Configuration
class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

	@Autowired
	PlayerRepository playerRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Override
	public void init(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(inputName-> {
			Player player = playerRepository.findByUserName(inputName);
			if (player!= null) {
				return new User(player.getUserName(), player.getPassword(),
						AuthorityUtils.createAuthorityList("USER"));
			} else {
				throw new UsernameNotFoundException("Unknown user: " + inputName);
			}
		});
	}

}


@Configuration
@EnableWebSecurity
class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/web/games.html").permitAll()
				.antMatchers("/web/**").permitAll()
				.antMatchers("/api/games").permitAll()
				.antMatchers("/api/games/**").permitAll()
				.antMatchers("/api/players").permitAll()
				.antMatchers("/api/game_view/*").permitAll()//.hasAuthority("USER")
				.antMatchers("/rest/*").permitAll()//denyAll()
				.anyRequest().permitAll();

		http.formLogin()
				.usernameParameter("username")
				.passwordParameter("password")
				.loginPage("/api/login");

		http.logout().logoutUrl("/api/logout");

		http.csrf().disable();

		// if user is not authenticated, just send an authentication failure response
		http.exceptionHandling().authenticationEntryPoint((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

		// if login is successful, just clear the flags asking for authentication
		http.formLogin().successHandler((req, res, auth) -> clearAuthenticationAttributes(req));

		// if login fails, just send an authentication failure response
		http.formLogin().failureHandler((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

		// if logout is successful, just send a success response
		http.logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());
	}

	private void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session != null) session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}
}