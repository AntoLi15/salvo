package com.codeoftheweb.salvo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Salvo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")

    private long id;

    private int turn;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "gamePlayer_id")
    private GamePlayer gamePlayer; //

    @ElementCollection
    @Column(name = "locations")
    private List<String> locationList = new ArrayList<>();

    public Salvo()   { }


    public Salvo(int turn, GamePlayer gamePlayer, List<String> locationList) {
        this.setTurn(turn);
        this.setGamePlayer(gamePlayer);
        this.locationList=locationList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }


    public GamePlayer getGamePlayer() {
        return gamePlayer;
    }

    public void setGamePlayer(GamePlayer gamePlayer) {
        this.gamePlayer = gamePlayer;
    }


    public List<String> getLocationList() {
        return locationList;
    }


    public void setLocations(List<String> locationList)
    {
        this.locationList = locationList;
    }

}
